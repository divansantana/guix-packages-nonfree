;; Currently requires the user to use the prewritten sudoers file to run steam in the container as an unprivileged user - this will be solved in the future either with a service that builds the sudoers file, and/or building and running a container using guix's container functionality which allows unprivileged users to build and run containers.
;;    Ideally we can use guix's container functionality, then this service can be repurposed to simply run steam in it's own user, and the containerised Steam can be a package that can be installed and run by anyone, meaning the service isn't required to install and run steam.

(define-module (pkill9 steam-services)
  #:use-module (ice-9 ftw) ;; for creating recursive list of directories of libs for FHS  #:use-module (guix download)
  #:use-module (srfi srfi-1) ;; For "remove" for delete-service
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix build-system trivial)
  #:use-module (guix packages)
  #:use-module (guix git-download) ;; For steam-isolated-with-chrootenv
  #:use-module (gnu system accounts) ;; For 'user-account'
  #:use-module (gnu system shadow) ;; For 'account-service-type'
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base)
  #:use-module (guix records) ;; define-record-type*
  #:use-module (pkill9 utils steam) ;; steam-chrootenv-wrapper-wrapper
  ;; sudoers-file-for-steam
  #:use-module (guix gexp)
  #:export (steam-chrootenv-configuration))

(define (steam-accounts user-name)
  (list  (user-account
         (name user-name)
         (group "users")
         (supplementary-groups
          ;; Added to video group so hardware acceleration works, otherwise libgl complains about not having permissions.
          (list "netdev" "video"))
;;         (system? #t)
         (home-directory (string-append "/home/" user-name))
         ;;(shell (file-append shadow "/sbin/nologin"))
         )))

(define-record-type* <steam-chrootenv-configuration>
  steam-chrootenv-configuration
  make-steam-chrootenv-configuration
  steam-chrootenv-configuration?
  (user-to-run-steam-as           steam-chrootenv-configuration-user
                                  (default "test")))

(define (steam-chrootenv-accounts-service config)
  (let* ((user-name (steam-chrootenv-configuration-user config)))
    (steam-accounts user-name)))

(define (steam-chrootenv-profile-service config)
  (let* ((user-to-run-steam-as (steam-chrootenv-configuration-user config)))
    (list (steam-chrootenv-wrapper-wrapper user-to-run-steam-as))))

(define-public steam-chrootenv-service-type
  (service-type (name 'steam-chrootenv)
                (extensions
                 (list (service-extension account-service-type
                                          steam-chrootenv-accounts-service)
                       (service-extension profile-service-type
                                          steam-chrootenv-profile-service)))
                (default-value (steam-chrootenv-configuration))))

(define-public (sudoers-file-for-steam user-to-run-steam-as)
  (mixed-text-file "sudoers"
                   "root ALL=(ALL) ALL\n"
                   "%wheel ALL=(ALL) ALL\n"
                   "ALL ALL=(ALL) NOPASSWD: " (steam-chrootenv-run-script user-to-run-steam-as) "\n"
                   "ALL ALL=(test) NOPASSWD:SETENV: ALL\n"))

(define-public steam-chrootenv-service
  ;;"Add a wrapper to the system profile to run steam in a chroot (for certain required files in the root directory) and as it's own user."
  (service steam-chrootenv-service-type))
