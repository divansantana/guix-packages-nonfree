(define-module (pkill9 utils steam)
  ;; For building the scripts
  #:use-module (pkill9 utils) ;; create-bash-script
  #:use-module (pkill9 packages steam steam-client) ;; steam
  #:use-module (guix packages)
  #:use-module (guix modules)
  #:use-module (guix download)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gawk)

  #:use-module (gnu packages admin)
  #:use-module (gnu packages xorg)
  ;; steam-chrootenv-wrapper-wrapper
  #:use-module (guix build-system trivial))


(define steam-chrootenv-tool-script-text
  "# CHROOTENV_DIR is the directory that stores the chroot environment.
# MIRRORDIRS is a space-separated string list of directories of the host system to bind-mount into the chrootenv.

CHROOTENV_DIR=\"/run/chrootenv/steam\"
MIRRORDIRS=\"/gnu /dev /dev/pts /dev/shm /proc /sys /home /var /run /tmp\"

function mirrordir_in_chrootenv () {
    for arg in $@ ; do
        $mkdir -p $CHROOTENV_DIR$arg
    done
}

function bindmount_to_chrootenv () {
    for arg in $@ ; do
        $mount --bind $arg $CHROOTENV_DIR$arg
    done
}

function create_chrootenv () {
    for arg in $(echo $MIRRORDIRS | xargs) ; do
        if $mount | $grep -q $CHROOTENV_DIR$arg ; then
            $echo $CHROOTENV_DIR$arg already has a directory mounted to it.
        else
        mirrordir_in_chrootenv $arg
        bindmount_to_chrootenv $arg
        fi
    done
    # Exra steam stuff
    $mkdir -p $CHROOTENV_DIR/usr/bin && $ln -s $env $CHROOTENV_DIR/usr/bin/env
    $mkdir -p $CHROOTENV_DIR/bin && $ln -s $bash $CHROOTENV_DIR/bin/bash
    $mkdir -p $CHROOTENV_DIR/bin && $ln -s $bash $CHROOTENV_DIR/bin/sh # Didn't know it needs this, but it does!
    $mkdir -p $CHROOTENV_DIR/lib && $ln -s $ld_linux_32 $CHROOTENV_DIR/lib/ld-linux.so.2
    $mkdir -p $CHROOTENV_DIR/lib64 && $ln -s $ld_linux_64 $CHROOTENV_DIR/lib64/ld-linux-x86-64.so.2
##    $mkdir -p $CHROOTENV_DIR/etc && $ln -s $ld_cache_for_steam $CHROOTENV_DIR/etc/ld.so.cache
##    $mkdir -p $CHROOTENV_DIR/etc && $ln -s $ld_conf_for_steam $CHROOTENV_DIR/etc/ld.so.conf
    $mkdir -p $CHROOTENV_DIR/etc && $cp -r /etc/passwd /etc/group /etc/sudoers /etc/pam.d /etc/static /etc/ssl $CHROOTENV_DIR/etc
##    $cp --no-clobber -r /etc/* $CHROOTENV_DIR/etc

}

function destroy_chrootenv () {
    for mountpoint in $(mount | $grep $CHROOTENV_DIR | $awk '{print $3}' | $xargs) ; do
        $umount -R $mountpoint
    done
}

function enter_chrootenv () {
    $chroot $CHROOTENV_DIR
}

function run_chrootenv () {
    $chroot $CHROOTENV_DIR \"$@\"
}

if [[ \"$1\" == \"create\" ]] ; then
    create_chrootenv
elif [[ \"$1\" == \"destroy\" ]] ; then
    destroy_chrootenv
elif [[ \"$1\" == \"enter\" ]] ; then
    enter_chrootenv
else
    echo Available commands: create, destroy, enter
fi\n")

(define (steam-chrootenv-run-script-text user-to-run-steam-as)
  (format #f
          "CHROOTENV_DIR=/run/chrootenv/steam
USER_TO_RUN_STEAM_AS=~a

if [[ \"$USER\" != \"root\" ]] ; then
    echo This script needs to be run as root using sudo
    exit 1
elif [[ -z \"$SUDO_USER\" ]] ; then
    echo This script needs to be run as root using sudo
    exit 1
fi

# Create the steam chrootenv.
$steamchrootenv create

# Allow local connections to this user's X session.
$sudo -u $SUDO_USER $xhost +local:

# Run steam in the steam chrootenv as the specified user.
$chroot /run/chrootenv/steam $sudo -iu $USER_TO_RUN_STEAM_AS STEAM_FRAME_FORCE_CLOSE=0 $steam \"$@\" 

# Destroy the steam chrootenv when steam closes.
$steamchrootenv destroy"
          user-to-run-steam-as))

(define* (steam-chrootenv-tool-script #:optional (glibc-package glibc))
  (let* ((glibc32 (package (inherit glibc-package)
                           (arguments
                            `(#:system "i686-linux"
                              ,@(package-arguments glibc-package)))))
         (executables-paths (list
                             (list coreutils "/bin/" "mkdir")
                             (list coreutils "/bin/" "cp")
                             (list util-linux "/bin/" "mount")
                             (list grep "/bin/" "grep")
                             (list coreutils "/bin/" "echo")
                             (list coreutils "/bin/" "ln")
                             (list gawk "/bin/" "awk")
                             (list findutils "/bin/" "xargs")
                             (list util-linux "/bin/" "umount")
                             (list coreutils "/bin/" "chroot")))
         (arbitrary-files-paths (list
                                 (list coreutils "/bin/env" "env")
                                 (list bash "/bin/bash" "bash")
                                 (list glibc32 "/lib/ld-linux.so.2" "ld_linux_32")
                                 (list glibc-package "/lib/ld-linux-x86-64.so.2" "ld_linux_64"))))
    (create-bash-script "steam-chrootenv-tool-script" steam-chrootenv-tool-script-text executables-paths arbitrary-files-paths)))

(define-public (steam-chrootenv-run-script user-to-run-steam-as)
  (let* ((paths (list
                 (list sudo "/bin/" "sudo")
                 (list coreutils "/bin/" "chroot")
                 (list xhost "/bin/" "xhost")
                 (list (steam-chrootenv-tool-script) "steamchrootenv")
                 (list steam "/bin/" "steam")))
         (script-text (steam-chrootenv-run-script-text user-to-run-steam-as)))
    
    (create-bash-script "steam-chrootenv-run-script" script-text paths)))

(define steam-icon
  (origin
    (method url-fetch)
    (uri "http://icons.iconarchive.com/icons/ampeross/smooth/128/Steam-icon.png")   
    (sha256
     (base32
      "1ngaiyzpj5gm9644l18maar6lg0hqjn9jryycm3x2768dmdjkhn8"))))

(define-public (steam-chrootenv-wrapper-wrapper user-to-run-steam-as)
  ;;This package is designed to be run globally - it runs steam as another user, regardless which user runs this script
  (package
   (name "steam-chrootenv-wrapper-wrapper")
   (version "0")
   (source #f)
   (build-system trivial-build-system)
   (inputs
    `(("steam-chrootenv-wrapper" ,(steam-chrootenv-run-script user-to-run-steam-as))
      ("steam-icon" ,steam-icon)))
   (propagated-inputs (package-propagated-inputs steam))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out (assoc-ref %outputs "out"))
               (steam-chrootenv-wrapper
                (assoc-ref %build-inputs "steam-chrootenv-wrapper")))
          (mkdir-p (string-append out "/bin"))
          (with-output-to-file
              (string-append out "/bin/steam-chrootenv-run")
            (lambda _
              (format #t ;; Todo: check that this script isn't being run as root, as this is basically an alias to run the steam wrapper as root. On the other hand, it shouldn't matter as steam itself can be run as root. Maybe put a check in here anyways? Will want to remove the wrapper stuff anyways. On the oter hand, xorg is still somewhat insecure so maybe best not to run as root.

                      "#!/bin/sh
sudo ~a"
                      steam-chrootenv-wrapper)))
          ;;Desktop file
          (mkdir-p (string-append out "/share/applications"))
          (with-output-to-file
              (string-append out "/share/applications/steam-chrootenv.desktop")
            (lambda _
              (format #t
                      "[Desktop Entry]
Type=Application
Name=Steam
Exec=~a %U
Terminal=false
icon=~a
Categories=Game;
Comment=Play steam games (Isolated)."
                      (string-append out "/bin/steam-chrootenv-run")
                      (assoc-ref %build-inputs "steam-icon"))))
          (chmod (string-append out "/bin/steam-chrootenv-run") #o755))
        )))
   (home-page "")
   (synopsis "A wrapper to run the steam-chrootenv-wrapper")
   (description "")
   (license #f)))

(steam-chrootenv-wrapper-wrapper "steam")
