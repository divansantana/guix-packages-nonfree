(define-module (pkill9 packages font-awesome-next)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix download)
  #:use-module (gnu packages fonts)
 )

(define-public font-awesome-newer
  (package (inherit font-awesome)
    (name "font-awesome")
    (version "5.0.13")
    (source
     (origin
       (method url-fetch)
       (uri "https://use.fontawesome.com/releases/v5.0.13/fontawesome-free-5.0.13.zip")
       (sha256
        (base32
         "1gvr4dv2gkgxskkg5kppp5v8r1rwprsww2mq9isy100pyad5mbjd"))))
    ))
