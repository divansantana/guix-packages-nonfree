(define-module (pkill9 packages unrar)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

(define-public unrar
(package
  (name "unrar")
  (version "5.6.8")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://www.rarlab.com"
             "/rar/"
             "unrarsrc-" version ".tar.gz"))
      (sha256
        (base32
          "1zrxvn8if5byz9v6axpmkl9c5qh9195azlhj35sjfj1m9b0hmk54"))))
  (arguments
   `(#:tests? #f
     #:make-flags `(,(string-append "DESTDIR=" %output))
     #:phases (modify-phases %standard-phases (delete 'configure))
     ))
  (build-system gnu-build-system)
  (home-page "")
  (synopsis "")
  (description "")
  (license "proprietary"))

 )
(package (inherit unrar))
