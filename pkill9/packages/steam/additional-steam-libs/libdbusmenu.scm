; doesn't generate libdbusmenu-gtk.so.4, only libdbusmenu-gtk3.so.4
(define-module (pkill9 packages steam additional-steam-libs libdbusmenu)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public libdbusmenu
(package
  (name "libdbusmenu")
  (version "16.04.0")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://launchpad.net/libdbusmenu/16.04/"
             version
             "/+download/libdbusmenu-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "12l7z8dhl917iy9h02sxmpclnhkdjryn08r8i4sr8l3lrlm4mk5r"))))
  (build-system gnu-build-system)
  (native-inputs
    `(("intltool" ,(@ (gnu packages glib) intltool))
      ("perl-xml-parser"
       ,(@ (gnu packages xml) perl-xml-parser))
      ("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("python" ,(@ (gnu packages python) python-2.7))
      ("valgrind"
       ,(@ (gnu packages valgrind) valgrind))
      ("glib:bin" ,(@ (gnu packages glib) glib) "bin")
      ("dbus-glib" ,(@ (gnu packages glib) dbus-glib))))
  (inputs
    `(("glib" ,(@ (gnu packages glib) glib))
      ("gtk+" ,(@ (gnu packages gtk) gtk+))
      ("gtk+" ,(@ (gnu packages gtk) gtk+-2))
      ("atk" ,(@ (gnu packages gtk) atk))
      ("libx11" ,(@ (gnu packages xorg) libx11))
      ("json-glib" ,(@ (gnu packages gnome) json-glib))
      ("gobject-introspection"
       ,(@ (gnu packages glib) gobject-introspection))
      ("vala" ,(@ (gnu packages gnome) vala))))
  (arguments `(#:tests? #f
	       #:configure-flags '("--with-gtk=2")
	       #:phases 
                 (modify-phases %standard-phases
                          (add-after 'unpack 'fix-introspection-install-dir
                                     (lambda* (#:key outputs #:allow-other-keys)
                                       (let ((out (assoc-ref outputs "out")))
                                         (substitute* '("configure")
                                                        ;"libs/pls/Makefile.in"
                                                        ;"libs/net/Makefile.in")
                                           (("INTROSPECTION_GIRDIR=.*")
                                            (string-append "INTROSPECTION_GIRDIR=" out "/share/gir-1.0/" "\n"))
                                           (("INTROSPECTION_TYPELIBDIR=.*")
                                            (string-append "INTROSPECTION_TYPELIBDIR=" out "/lib/girepository-1.0/" "\n")))))))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
