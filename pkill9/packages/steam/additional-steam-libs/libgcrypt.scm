;todo: Make libs32 inherit arguments from libs64. Generally clean up everythign to make it easier to read and understand, maybe put required libs for steam in their own files so it's easy to find them.
(define-module (pkill9 packages steam additional-steam-libs libgcrypt)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix profiles)
  #:use-module (guix derivations)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (gnu packages base)
  #:use-module (guix build utils)
  #:use-module (guix build-system trivial)

  #:use-module (guix git-download)
  #:use-module (gnu packages video)
  #:use-module (gnu packages gnupg)
)

(define-public libgcrypt-1.5.3
  (package (inherit libgcrypt)
    (name "libgcrypt")
    (version "1.5.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://gnupg/libgcrypt/libgcrypt-"
                                 version ".tar.bz2"))
       (sha256
        (base32
         "1lar8y3lh61zl5flljpz540d78g99h4d5idfwrfw8lm3gm737xdw"))))
    ))
