(define-module (pkill9 packages steam additional-steam-libs symlink-hacks)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial))

;; link udev lib to newer udev (would be better to build older udev tho). libudev is vital because steam fails to open if it doesn't exist. This is a bad idea to symlink incompatible library versions because errors and strange behaviour may occur without any warnings.

(define-public udev-symlink
  (package
    (name "udev-symlink")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (inputs `(("eudev" ,(@ (gnu packages linux) eudev))))
    (arguments
      `(#:modules ((guix build utils))
	#: builder (begin
		     (use-modules (guix build utils))
                     (let ((out (assoc-ref %outputs "out")))
		          (mkdir-p (string-append out "/lib"))
			  (symlink (string-append (assoc-ref %build-inputs "eudev") "/lib/libudev.so.1")
				   (string-append out "/lib/libudev.so.0"))
			  ))))
    (home-page #f)
    (synopsis
      "Symlink from libudev.so.0 to libudev.so.1")
    (description synopsis)
    (license #f)))


(define-public libcurl-gnutls-symlink
  (package
    (name "libcurl-gnutls-symlink")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (inputs `(("curl" ,(@ (gnu packages curl) curl))))
    (arguments
      `(#:modules ((guix build utils))
	#: builder (begin
		     (use-modules (guix build utils))
                     (let ((out (assoc-ref %outputs "out")))
		          (mkdir-p (string-append out "/lib"))
			  (symlink (string-append (assoc-ref %build-inputs "curl") "/lib/libcurl.so.4")
				   (string-append out "/lib/libcurl-gnutls.so.4"))
			  ))))
    (home-page #f)
    (synopsis
      "Symlink from libcurl-gnutls.so.4 to libcurl.so.4")
    (description
      "Symlink from libcurl-gnutls.so.4 to libcurl.so.4")
    (license #f)))

(define-public libpcre-symlink
  (package
    (name "libpcre-symlink")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (inputs `(("pcre" ,(@ (gnu packages pcre) pcre))))
    (arguments
      `(#:modules ((guix build utils))
	#: builder (begin
		     (use-modules (guix build utils))
                     (let ((out (assoc-ref %outputs "out")))
		          (mkdir-p (string-append out "/lib"))
			  (symlink (string-append (assoc-ref %build-inputs "pcre") "/lib/libpcre.so.1")
				   (string-append out "/lib/libpcre.so.3"))
			  ))))
    (home-page #f)
    (synopsis
      "Symlink from libpcre.so.3 to libpcre.so.1")
    (description synopsis)
    (license #f)))
