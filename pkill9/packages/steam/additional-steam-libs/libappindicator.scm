(define-module (pkill9 packages steam additional-steam-libs libappindicator)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public libappindicator
(package
  (name "libappindicator")
  (version "12.10.0")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://launchpad.net/libappindicator/12.10/"
             version
             "/+download/libappindicator-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "17xlqd60v0zllrxp8bgq3k5a1jkj0svkqn8rzllcyjh8k0gpr46m"))))
  (build-system gnu-build-system)
  (native-inputs
    `(("glib:bin" ,(@ (gnu packages glib) glib) "bin")
      ("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))))
  (inputs
    `(("gtk+" ,(@ (gnu packages gtk) gtk+-2))
      ("glib" ,(@ (gnu packages glib) glib))
      ("dbus-glib" ,(@ (gnu packages glib) dbus-glib))
      ("python" ,(@ (gnu packages python) python-2))
      ("python2-pygtk" ,(@ (gnu packages gtk) python2-pygtk))
      ("python2-pygobject" ,(@ (gnu packages glib) python2-pygobject))
      ("vala" ,(@ (gnu packages gnome) vala)) ; this is purely to amend build error, vala is't actually required by configure phase
      ("libdbusmenu" ,(@ (pkill9 packages steam additional-steam-libs libdbusmenu) libdbusmenu))
      ("libindicator" ,(@ (pkill9 packages steam additional-steam-libs libindicator) libindicator))
      ("gobject-introspection"
       ,(@ (gnu packages glib) gobject-introspection))))
  ; need to replace PYGTK_CODEGEN="$PYTHON `$PKG_CONFIG --variable=codegendir pygtk-2.0`/codegen.py" in configure with path to actual codegen.py which is in /bin
  (arguments
   `(#:tests? #f ;tests hardcoded to use /bin/sh, but because I'm lazy I just disable the tests. Really should modify the tests to use store path to bash.
     #:phases
     (modify-phases %standard-phases
                    (add-after 'unpack 'fix-codegen-path
                               (lambda _
                                 (substitute* "configure"
                                              (("PYGTK_CODEGEN=.*") "PYGTK_CODEGEN=pygtk-codegen-2.0\n"))))
		    (add-after 'unpack 'disable-fail-on-error
			       (lambda _
				 (substitute* "src/Makefile.in"
					      (("-Werror") "")))))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
