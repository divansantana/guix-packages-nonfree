;todo: Make libs32 inherit arguments from libs64. Generally clean up everythign to make it easier to read and understand, maybe put required libs for steam in their own files so it's easy to find them.
(define-module (pkill9 packages steam additional-steam-libs libva)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix profiles)
  #:use-module (guix derivations)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (gnu packages base)
  #:use-module (guix build utils)
  #:use-module (guix build-system trivial)

  #:use-module (guix git-download)
  #:use-module (gnu packages video)
  #:use-module (gnu packages gnupg)
)

; ========================= \/ DEPENDENCIES \/ ====================

(define-public libva-1
  (package (inherit libva)
    (name "libva")
    (version "1.8.3")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/intel/libva/releases/download/1.8.3/libva-1.8.3.tar.bz2")
       (sha256
        (base32
         "16xbk0awl7wp0vy0nyjvxk11spbw25mp8kwd9bmhd6x9xffi5vjn"))))
    ))
