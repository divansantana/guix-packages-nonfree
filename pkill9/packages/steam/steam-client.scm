;;The steam package requires that the dynamic linker (glibc) is available in /lib. The location can't be changed because Steam overwrites itself if it is patched.
;;
;;todo: Generally clean up everythign to make it easier to read and understand, maybe put required libs for steam in their own files so it's easy to find them.
;;Need to add coreutils to PATH as steamwebhelper.sh is failing without it and it runs in a chrootenv which i don't think inherits the user environment, then again python3 gets found, which is odd because coreutils is already in the 'test' user's profile and it doesn't get found.
;;Gmod is segfaulting because it can't find LiberationMono-Regular.ttf, and DejaVuSans.ttf - it's in the package font-liberation and font-dejavu. Need to add this to the environment automatically somehow, preferably by adding it to the steam-libs union thing and use that as a singl profile to include everything needed in the PATH and the fonts etc. Installing this font into the profile is working, so I'll just add fonts as propagated inputs for now
;;
;;Old notes from steam-libs.scm:
;;!!!! WARNING: GST-PLUGINS-BASE FAILS TO BUILD FOR i686-linux, SO IT HAS BEEN DISABLED ALONG WITH GST-PLUGINS-UGLY WHICH DEPENDS ON THE FORMER!!!!
;;todo: Make libs32 inherit arguments from libs64. Generally clean up everythign to make it easier to read and understand, maybe put required libs for steam in their own files so it's easy to find them.
;;
;;Old notes from steam-chrootenv-wrapper.scm:
;; Problem: ldconfig ignores the name of the symlink when generating the cache
;;          Idea: Incorporate LD_LIBRARY_PATH into the chrootenv wrapper when running steam - I tested this, it works
;; Problem: Steam doesn't find libGL.so.1 when relying on ld.so.cache to provide the libs
;;
;; Other notes on steam + game issues:
;; Fix sven-coop invisible fonts: https://www.reddit.com/r/linux_gaming/comments/bhba6i/sven_coop_menu_dont_have_text_solve_by_installing/

(define-module (pkill9 packages steam steam-client)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)
  ;; steam libs unions
  #:use-module (pkill9 packages steam additional-steam-libs symlink-hacks) ;; symlink packages
  #:use-module (pkill9 utils)) ;; package-output->package

(define steam-client-libs
  `(("libxcomposite"
       ,(@ (gnu packages xorg) libxcomposite))
      ("libxtst" ,(@ (gnu packages xorg) libxtst))
      ("libxrandr" ,(@ (gnu packages xorg) libxrandr))
      ("libxext" ,(@ (gnu packages xorg) libxext))
      ("libx11" ,(@ (gnu packages xorg) libx11))
      ("libxfixes" ,(@ (gnu packages xorg) libxfixes))
      ("glib" ,(@ (gnu packages glib) glib))
      ("gtk+" ,(@ (gnu packages gtk) gtk+-2))
      ("bzip2" ,(@ (gnu packages compression) bzip2))
      ("zlib" ,(@ (gnu packages compression) zlib))
      ("gdk-pixbuf" ,(@ (gnu packages gtk) gdk-pixbuf))
      ("libxinerama"
       ,(@ (gnu packages xorg) libxinerama))
      ("libxdamage"
       ,(@ (gnu packages xorg) libxdamage))
      ("libxcursor"
       ,(@ (gnu packages xorg) libxcursor))
      ("libxrender"
       ,(@ (gnu packages xorg) libxrender))
      ("libxscrnsaver"
       ,(@ (gnu packages xorg) libxscrnsaver))
      ("libxxf86vm"
       ,(@ (gnu packages xorg) libxxf86vm))
      ("libxi" ,(@ (gnu packages xorg) libxi))
      ("libsm" ,(@ (gnu packages xorg) libsm))
      ("libice" ,(@ (gnu packages xorg) libice))
      ("gconf" ,(@ (gnu packages gnome) gconf))
      ("freetype"
       ,(@ (gnu packages fontutils) freetype))
      ("curl" ,(@ (gnu packages curl) curl))
      ("libcurl-gnutls-symlink" ,libcurl-gnutls-symlink)
      ("libpcre-symlink" ,libpcre-symlink)
      ("nspr" ,(@ (gnu packages nss) nspr))
      ("nss" ,(@ (gnu packages nss) nss))
      ("fontconfig"
       ,(@ (gnu packages fontutils) fontconfig))
      ("cairo" ,(@ (gnu packages gtk) cairo))
      ("pango" ,(@ (gnu packages gtk) pango))
      ("expat" ,(@ (gnu packages xml) expat))
      ("dbus" ,(@ (gnu packages glib) dbus))
      ("cups" ,(@ (gnu packages cups) cups))
      ("libcap" ,(@ (gnu packages linux) libcap))
      ("sdl2" ,(@ (gnu packages sdl) sdl2))
      ("libusb" ,(@ (gnu packages libusb) libusb))
      ("dbus-glib" ,(@ (gnu packages glib) dbus-glib))
      ("atk" ,(@ (gnu packages gtk) atk))
      ("at-spi2-atk" ,(@ (gnu packages gtk) at-spi2-atk)) ;; Required by steam client beta
      ("at-spi2-core" ,(@ (gnu packages gtk) at-spi2-core)) ;; Required by steam client beta
      ("eudev" ,(@ (gnu packages linux) eudev))
      ("udev-symlink" ,udev-symlink)
      ("network-manager"
       ,(@ (gnu packages gnome) network-manager))
      ("gcc:lib" ,(package-output->package (@ (gnu packages gcc) gcc-7) "lib"))
      ("pulseaudio"
       ,(@ (gnu packages pulseaudio) pulseaudio))
      ("libva" ,(@ (gnu packages video) libva))
      ("libappindicator" ,(@ (gnu packages freedesktop) libappindicator)) ; for systray
      ("openal" ,(@ (gnu packages audio) openal))
      ("alsa-lib" ,(@ (gnu packages linux) alsa-lib))
      ("mesa" ,(@ (gnu packages gl) mesa))
      ("libffi" ,(@ (gnu packages libffi) libffi))
      ("libselinux" ,(@ (gnu packages selinux) libselinux))
      ("vulkan-loader" ,(@ (gnu packages vulkan) vulkan-loader))))

(define steam-gameruntime-libs
      `(("libxmu" ,(@ (gnu packages xorg) libxmu))
      ("libxcb" ,(@ (gnu packages xorg) libxcb))
      ("glu" ,(@ (gnu packages gl) glu))
      ("util-linux"
       ,(@ (gnu packages linux) util-linux))
      ("libogg" ,(@ (gnu packages xiph) libogg))
      ("libvorbis"
       ,(@ (gnu packages xiph) libvorbis))
      ("sdl" ,(@ (gnu packages sdl) sdl))
      ("sdl2-image" ,(@ (gnu packages sdl) sdl2-image))
      ("glew" ,(@ (gnu packages gl) glew))
      ("openssl" ,(@ (gnu packages tls) openssl))
      ("libidn" ,(@ (gnu packages libidn) libidn))
      ("tbb" ,(@ (gnu packages tbb) tbb))
      ("flac" ,(@ (gnu packages xiph) flac))
      ("freeglut" ,(@ (gnu packages gl) freeglut))
      ("libjpeg" ,(@ (gnu packages image) libjpeg))
      ("libpng" ,(@ (gnu packages image) libpng))
      ("libsamplerate"
       ,(@ (gnu packages pulseaudio) libsamplerate))
      ("libmikmod" ,(@ (gnu packages sdl) libmikmod))
      ("libtheora" ,(@ (gnu packages xiph) libtheora))
      ("libtiff" ,(@ (gnu packages image) libtiff))
      ("pixman" ,(@ (gnu packages xdisorg) pixman))
      ("speex" ,(@ (gnu packages xiph) speex))
      ("sdl-image" ,(@ (gnu packages sdl) sdl-image))
      ("sdl-ttf" ,(@ (gnu packages sdl) sdl-ttf))
      ("sdl-mixer" ,(@ (gnu packages sdl) sdl-mixer))
      ("sdl2-ttf" ,(@ (gnu packages sdl) sdl2-ttf))
      ("sdl2-mixer" ,(@ (gnu packages sdl) sdl2-mixer))
      ("gstreamer"
       ,(@ (gnu packages gstreamer) gstreamer))
      ;!!TEST CURRENTLY FAILS ON i686-linux!!:
      ;("gst-plugins-base"
      ; ,(@ (gnu packages gstreamer) gst-plugins-base))
      ("glu" ,(@ (gnu packages gl) glu))
      ("libcaca" ,(@ (gnu packages video) libcaca))
      ("libcanberra"
       ,(@ (gnu packages libcanberra) libcanberra))
      ("libgcrypt" ,(@ (gnu packages gnupg) libgcrypt)) ;hmm, looks like duplicate
      ("libvpx" ,(@ (gnu packages video) libvpx))
      ("librsvg" ,(@ (gnu packages gnome) librsvg))
      ("libxft" ,(@ (gnu packages xorg) libxft))
      ("libvdpau" ,(@ (gnu packages video) libvdpau))
      ;("gst-plugins-ugly"
      ; ,(@ (gnu packages gstreamer) gst-plugins-ugly))
      ("libdrm" ,(@ (gnu packages xdisorg) libdrm))
      ("xkeyboard-config"
       ,(@ (gnu packages xorg) xkeyboard-config))
      ("libpciaccess"
       ,(@ (gnu packages xorg) libpciaccess))
      ;;("ffmpeg" ,(@ (gnu packages video) ffmpeg-3.4)) ;;fails to build on i686-linux due to failing test (libvf-fits or something)
      ("libpng" ,(@ (gnu packages image) libpng-1.2))
      ("libgcrypt"
       ,(@ (pkill9 packages steam additional-steam-libs libgcrypt) libgcrypt-1.5.3))
      ("libgpg-error"
       ,(@ (gnu packages gnupg) libgpg-error))
      ("sqlite" ,(@ (gnu packages sqlite) sqlite))))

(define (inputs->packages inputs)
  (map (lambda (input)
         (list-ref input 1))
       inputs))

(define-public steam-libs-64
  (union "steam-libs-64"
         (append (inputs->packages steam-client-libs)
                 (inputs->packages steam-gameruntime-libs))
         #:options '(#:system "x86_64-linux")))

(define-public steam-libs-32
  (union "steam-libs-32"
         (append (inputs->packages steam-client-libs)
                 (inputs->packages steam-gameruntime-libs))
         #:options '(#:system "i686-linux")))

(define-public steam
  (package
    (name "steam")
    (version "1.0.0.52")
    (source
      (origin
        (method url-fetch)
        (uri (string-append
               "http://repo.steampowered.com/steam/archive/precise/steam_"
                 version ".tar.gz"))
        (sha256
          (base32
            "014k00gc89pafzr3jq9l0jv39106jpjd2vsd164f1w3vb6gilfyx"))))
    (inputs `(("steam-libs-32" ,steam-libs-32)
	      ("steam-libs-64" ,steam-libs-64)
              ("glibc" ,(@ (gnu packages base) glibc))
              ("python" ,(@ (gnu packages python) python-3))
	      ))
    (propagated-inputs
     `(("font-liberation" ,(@ (gnu packages fonts) font-liberation)) ;; GMod segfaults if it doesn't find these fonts
       ("font-dejavu" ,(@ (gnu packages fonts) font-dejavu)))) ;; GMod segfaults if it doesn't find these fonts
    (arguments
     `(#:system "i686-linux" ; to get 32 bit glibc
       #:tests? #f
       #:make-flags (list "PREFIX=" (string-append "DESTDIR=" (assoc-ref %outputs "out")))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'patch-startscript
           ;; the script uses it's own name to determine the package, wrap-program interferes with this however
           (lambda _
	       (substitute* "steam"
;07:48pm <rekado_> or: "VARIABLE=\"\\$\\{0##\\*/\\}\""
                 (("STEAMPACKAGE=.*") "STEAMPACKAGE=steam\n"))
	   ;change references of /usr to the store path
	       (substitute* "steam"
                 (("/usr") (assoc-ref %outputs "out")))
               #t))
         (add-after 'unpack 'patch-desktop-file
           ;; the desktop file hardcodes /usr/lib, so guix doesn't patch it
           (lambda _
	       (substitute* "steam.desktop"
                 (("Exec=/usr/bin/steam") "Exec=steam"))
               #t))
         (add-after 'install-binaries 'remove-unneccessary-file
           ;; the desktop file hardcodes /usr/lib, as a result guix doesn't patch it
           (lambda _
	       (delete-file (string-append (assoc-ref %outputs "out") "/bin/steamdeps"))
               #t))
         (add-after 'install-binaries 'wrap-startscript
           (lambda* (#:key outputs inputs #:allow-other-keys)
             (let* (
         	   (out (assoc-ref outputs "out"))
                   (bin (string-append out "/bin/steam"))
                   (steam-libs-32 (assoc-ref inputs "steam-libs-32"))
                   (steam-libs-64 (assoc-ref inputs "steam-libs-64"))
                   (glibc (assoc-ref inputs "glibc"))
                   (python (assoc-ref inputs "python"))
         	   )
               (wrap-program bin
                 `("LD_LIBRARY_PATH" ":" suffix
                    (,(string-append steam-libs-64 "/lib:"
				     steam-libs-32 "/lib:"
				     steam-libs-64 "/lib/nss:"
				     steam-libs-32 "/lib/nss:"
				     "$HOME/.steam-enabled-bundled-libs/lib" ; give access to specified bundled libs
				     )))
                 `("PATH" ":" prefix
                   (,(string-append glibc "/bin") ; for 32 bit ldd so steam finds libc.so.6
                    ,(string-append python "/bin")))
		 '("STEAM_RUNTIME" = ("0"))) ;bundled mesa doesn't work.
		 ;'("SDL_AUDIODRIVER" = ("alsa"))) ;To get Garry's Mod audio working. !! Breaks DEFCON !!
               #t)))
      )))
    (build-system gnu-build-system)
    (home-page "https://store.steampowered.com")
    (synopsis "Application for managing and playing games on Steam")
    (description "Application for managing and playing games on Steam")
    (license #f)))

steam
