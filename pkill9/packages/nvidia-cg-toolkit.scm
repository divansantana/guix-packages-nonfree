(define-module (pkill9 packages nvidia-cg-toolkit)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses)
 )

(define-public nvidia-cg-toolkit
(package
  (name "nvidia-cg-toolkit")
  (version "3.1.0013")
  (source
    (origin
      (method url-fetch)
      (uri "http://developer.download.nvidia.com/cg/Cg_3.1/Cg-3.1_April2012_x86_64.tgz")
      (sha256
        (base32
          "0y4qms4lm9xiix93g45337rx5nrp0y3gb0x0avyv7l9qrkk03zz8"))))
  (build-system trivial-build-system)
  (native-inputs
    `(("patchelf" ,(@ (gnu packages elf) patchelf))
      ("tar" ,(@ (gnu packages base) tar))
      ("gzip" ,(@ (gnu packages compression) gzip))))
  (inputs
    `(("bash" ,(@ (gnu packages bash) bash)) ; for 'wrap-program', so it sets correct shebang
      ("glibc" ,(@ (gnu packages base) glibc))
      ("gcc" ,(@ (gnu packages gcc) gcc-8) "lib")
      ("glu" ,(@ (gnu packages gl) glu))
      ("libice" ,(@ (gnu packages xorg) libice))
      ("libsm" ,(@ (gnu packages xorg) libsm))
      ("libx11" ,(@ (gnu packages xorg) libx11))
      ("libxext" ,(@ (gnu packages xorg) libxext))
      ("libxi" ,(@ (gnu packages xorg) libxi))
      ("libxmu" ,(@ (gnu packages xorg) libxmu))
      ("libxt" ,(@ (gnu packages xorg) libxt))
      ("mesa" ,(@ (gnu packages gl) mesa))))
  (arguments
   `(#:modules ((guix build utils))
     #:builder
     (begin
       (use-modules (guix build utils))
       (let* ((out (assoc-ref %outputs "out"))
	      (source (assoc-ref %build-inputs "source"))
	      (tar (string-append (assoc-ref %build-inputs "tar") "/bin/tar"))
	      (patchelf (string-append (assoc-ref %build-inputs "patchelf") "/bin/patchelf"))
	      (glibc-interpreter (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")))
	      
	      ;Extract
	      (setenv "PATH" (string-append (assoc-ref %build-inputs "gzip") "/bin"))
	      (invoke tar "xvf" source)
	      (setenv "PATH" "") ;reset PATH

	      ;Move files into package layout
              (copy-recursively "usr/bin" "bin")
	      (copy-recursively "usr/include" "include")
	      (copy-recursively "usr/lib64" "lib") ;; Will need to change this depending on architecture downloaded
	      (copy-recursively "usr/local/Cg/docs" "share/doc/Cg")
	      (copy-recursively "usr/local/Cg/examples" "share/doc/Cg/examples")
	      (copy-file "usr/local/Cg/README" "share/doc/Cg/README")
	      (delete-file-recursively "usr")

	      ;Change binaries' interpreter paths
	      (invoke patchelf "--set-interpreter" glibc-interpreter "bin/cgc")
	      (invoke patchelf "--set-interpreter" glibc-interpreter "bin/cgfxcat")
	      (invoke patchelf "--set-interpreter" glibc-interpreter "bin/cginfo")
	      
	      ; install package - must be done before wrapping, otherwise the wrapper points to build directory paths
	      (copy-recursively "." out)

	      ;Wrap binaries to point them to native libraries, and the libraries provided. 'cgc' doesn't require any libraries according to ldd, but I will give it the Cg libs just in case, because even though cginfo says it only need gcc_s.so from ldd, it uses those libGc.so libraries itself. Who knows what proprietary software expects...
	      (chdir out)
              (setenv "PATH" (string-append (assoc-ref %build-inputs "bash") "/bin")) ; To create shebangs
	      (wrap-program "bin/cgc"
			    `("LD_LIBRARY_PATH" ":" suffix (,(string-append out "/lib"))))
	      (wrap-program "bin/cginfo" ;this also need gcc_s.so
			    `("LD_LIBRARY_PATH" ":" suffix (,(string-append out "/lib")
							    ,(string-append (assoc-ref %build-inputs "gcc") "/lib"))))
	      (wrap-program "bin/cgfxcat"
			    `("LD_LIBRARY_PATH" ":" suffix
			      ,(cons* (string-append out "/lib")
				(map
				 (lambda (input)
				  (string-append (assoc-ref %build-inputs input) "/lib"))
				  '("glu"
				    "libice"
				    "libsm"
				    "libx11"
				    "libxext"
				    "libxi"
				    "libxmu"
				    "libxt"
				    "mesa")))
			      ))
	      (setenv "PATH" "") ;reset PATH

	      ))))
  (supported-systems (list "x86_64-linux"))
  (home-page "https://developer.nvidia.com/cg-toolkit")
  (synopsis "The Cg Toolkit is a legacy NVIDIA toolkit no longer under active development or support.")
  (description "The Cg Toolkit is a legacy NVIDIA toolkit no longer under active development or support. NVIDIA was proud to introduce programmable shading with Cg, which supported dozens of different OpenGL and DirectX profile targets. It allowed developers to incorporate interactive effects within 3D applications and share them among other Cg applications, across graphics APIs, and most operating systems (Windows XP, Vista and Windows 7, Mac OS X for Leopard, Snow Leopard & Lion, Linux 32-bit & 64-bit) as well as balance effect complexities with client GPU capabilities.")
  (license "nonfree"))

 )
(package (inherit nvidia-cg-toolkit))
