;;todo:
;; Add inputs properly, don't rely on random profile.
;; use set-path-environment-variable instead of setenv
;; add propagated inputs for xdg-mime and xdg-settings
;; Add correct license
(define-module (pkill9 packages netflix)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages bash)
  ;#:use-module (guix licenses)
  ;for fontconfig:
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages pkg-config)
  ;unsure of neccessity:
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  )

(define-public fontconfig-2.12.6
  (package
   (name "fontconfig")
   (version "2.12.6")
   (source (origin
            (method url-fetch)
            (uri (string-append
                   "https://www.freedesktop.org/software/fontconfig/release/fontconfig-"
                   version ".tar.bz2"))
            (patches (search-patches "fontconfig-remove-debug-printf.patch"))
            (sha256 (base32
                     "05zh65zni11kgnhg726gjbrd55swspdvhqbcnj5a5xh8gn03036g"))))
   (build-system gnu-build-system)
   (propagated-inputs `(("expat" ,expat)
                        ("freetype" ,freetype)))
   (inputs `(("gs-fonts" ,gs-fonts)))
   (native-inputs
    `(("gperf" ,gperf)
      ("pkg-config" ,pkg-config)))
   (arguments
    `(#:configure-flags
      (list "--with-cache-dir=/var/cache/fontconfig"
            ;; register gs-fonts as default fonts
            (string-append "--with-default-fonts="
                           (assoc-ref %build-inputs "gs-fonts")
                           "/share/fonts")

            ;; Register fonts from user and system profiles.
            (string-append "--with-add-fonts="
                           "~/.guix-profile/share/fonts,"
                           "/run/current-system/profile/share/fonts")

            ;; python is not actually needed
            "PYTHON=false")
      #:phases
      (modify-phases %standard-phases
        (replace 'install
                 (lambda _
                   ;; Don't try to create /var/cache/fontconfig.
                   (zero? (system* "make" "install"
                                   "fc_cachedir=$(TMPDIR)"
                                   "RUN_FC_CACHE_TEST=false")))))))
   (synopsis "Library for configuring and customizing font access")
   (description
    "Fontconfig can discover new fonts when installed automatically;
perform font name substitution, so that appropriate alternative fonts can
be selected if fonts are missing;
identify the set of fonts required to completely cover a set of languages;
have GUI configuration tools built as it uses an XML-based configuration file;
efficiently and quickly find needed fonts among the set of installed fonts;
be used in concert with the X Render Extension and FreeType to implement
high quality, anti-aliased and subpixel rendered text on a display.")
   ; The exact license is more X11-style than BSD-style.
   (license (license:non-copyleft "file://COPYING"
                       "See COPYING in the distribution."))
   (home-page "http://www.freedesktop.org/wiki/Software/fontconfig")))


; Downloads and wraps Chrome
; latest version txt files at https://omahaproxy.appspot.com/all?os=linux&channel=stable
; https://github.com/voidlinux/void-packages/blob/master/srcpkgs/google-chrome/template
; guix environment --ad-hoc libxcomposite libxtst nss nspr cups libxrandr libxscrnsaver alsa-lib gcc:lib libxcursor libxdamage gobject-introspection glib dbus expat pango cairo atk gtk+ gdk-pixbuf -- sh -c "LD_LIBRARY_PATH=\$GUIX_ENVIRONMENT/lib:\$GUIX_ENVIRONMENT/lib/nss ./chrome"
; FONTCONFIG_PATH=$GUIX_ENVIRONMENT/etc/fonts LD_LIBRARY_PATH=$GUIX_ENVIRONMENT/lib:$GUIX_ENVIRONMENT/lib/nss:$(pwd):$(guix build fontconfig)/lib:/run/current-system/profile/lib ./chrome netflix.com
(define-public google-chrome
(package
  (name "google-chrome")
  (version "current")
  (source
    (origin
      (method url-fetch)
      (uri "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb")
      (sha256
       (base32
        "1fbn9vkz4kf1dhivi1sfnfi8dady9qjmfr44hvfmb8aibh9kzc8w"
        ))))
  (build-system trivial-build-system)
  (native-inputs
    `(("patchelf" ,(@ (gnu packages elf) patchelf))
      ("tar" ,(@ (gnu packages base) tar))
      ("xz" ,(@ (gnu packages compression) xz))
      ("binutils" ,(@ (gnu packages base) binutils))))
  (inputs
    `(("libxcomposite"
       ,(@ (gnu packages xorg) libxcomposite))
      ("libxtst" ,(@ (gnu packages xorg) libxtst))
      ("nss" ,(@ (gnu packages nss) nss))
      ("nspr" ,(@ (gnu packages nss) nspr))
      ("cups" ,(@ (gnu packages cups) cups))
      ("libxrandr" ,(@ (gnu packages xorg) libxrandr))
      ("libxscrnsaver"
       ,(@ (gnu packages xorg) libxscrnsaver))
      ("alsa-lib" ,(@ (gnu packages linux) alsa-lib))
      ("gcc" ,(@ (gnu packages gcc) gcc-7) "lib")
      ("libxcursor"
       ,(@ (gnu packages xorg) libxcursor))
      ("libxdamage"
       ,(@ (gnu packages xorg) libxdamage))
      ("gobject-introspection"
       ,(@ (gnu packages glib) gobject-introspection))
      ("glib" ,(@ (gnu packages glib) glib))
      ("dbus" ,(@ (gnu packages glib) dbus))
      ("expat" ,(@ (gnu packages xml) expat))
      ("pango" ,(@ (gnu packages gtk) pango))
      ("cairo" ,(@ (gnu packages gtk) cairo))
      ("atk" ,(@ (gnu packages gtk) atk))
      ("gtk+" ,(@ (gnu packages gtk) gtk+))
      ("gdk-pixbuf" ,(@ (gnu packages gtk) gdk-pixbuf))
      ("pulseaudio" ,(@ (gnu packages pulseaudio) pulseaudio))
      ("fontconfig" ,fontconfig-2.12.6)
      ("bash" ,(@ (gnu packages bash) bash))
      ("glibc" ,(@ (gnu packages base) glibc))))
  (arguments
   `(#:modules ((guix build utils))
     #:builder
     (begin
       (use-modules (guix build utils))
       (let* ((output (assoc-ref %outputs "out"))
              (source (assoc-ref %build-inputs "source"))
              ;(working-dir (string-append (getcwd) "/package"))
              (working-dir output)
              (ar (string-append (assoc-ref %build-inputs "binutils") "/bin/ar"))
              (tar (string-append (assoc-ref %build-inputs "tar") "/bin/tar"))
              (patchelf (string-append (assoc-ref %build-inputs "patchelf") "/bin/patchelf")))

         ; Extraction phase
         (mkdir-p working-dir)
         (setenv "PATH" (string-append (assoc-ref %build-inputs "xz") "/bin"))
         (zero? (system* ar "x" source "data.tar.xz"))
         (zero? (system* tar "xvf" "data.tar.xz" "-C" working-dir "./opt"))

         ; Patching phase - currently fails.
         (invoke patchelf "--set-interpreter"
                         (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")
                         (string-append working-dir "/opt/google/chrome/chrome"))
         (invoke patchelf "--set-interpreter"
                         (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")
                         (string-append working-dir "/opt/google/chrome/nacl_helper"))

         ; Wrapping phase - give it libraries and some other envars
         (setenv "PATH" (string-append (assoc-ref %build-inputs "bash") "/bin"))
         (wrap-program (string-append working-dir "/opt/google/chrome/chrome")
                       `("LD_LIBRARY_PATH" ":" prefix ("/run/current-system/profile/lib")))
         (wrap-program (string-append working-dir "/opt/google/chrome/chrome")
                       `("LD_LIBRARY_PATH" ":" prefix (,(string-append
                                                         (assoc-ref %build-inputs "nss")
                                                         "/lib/nss"))))
         (wrap-program (string-append working-dir "/opt/google/chrome/chrome")
                `("LD_LIBRARY_PATH" ":" prefix
                  ,(map (lambda (input)
                          (string-append (assoc-ref %build-inputs input)
                                         "/lib"))
                        '("libxcomposite" "libxtst" "nss" "nspr"
                          "cups" "libxrandr" "libxscrnsaver" "alsa-lib"
                          "gcc" "libxcursor" "libxdamage"
                          "gobject-introspection" "glib" "dbus" "expat"
                          "pango" "cairo" "atk" "gtk+" "gdk-pixbuf"
                          "pulseaudio")))
                ; Reads fontconfig config (version 1.12.6 compatible required)
                `("FONTCONFIG_PATH" ":" prefix (,(string-append
                                                (assoc-ref %build-inputs "fontconfig")
                                                "/etc/fonts"))))

      ; Polishing phase
         (mkdir-p (string-append working-dir "/bin"))
         (symlink
          "../opt/google/chrome/chrome"
          (string-append working-dir "/bin/chrome"))
       ))))
  (home-page "https://www.google.com")
  (synopsis "Google Chrome web browser.")
  (description "Google Chrome web browser.")
  (license #f))

 )

(define %netflix-icon
      (origin
        (method url-fetch)
        (uri "http://www.iconarchive.com/download/i106070/papirus-team/papirus-apps/netflix.svg")
        (sha256
          (base32
            "06n3crmfc3k8yahybic399p832vzj5afrdqvlizrk8lbk3plrjd2"))))

(define %amazon-icon
      (origin
        (method url-fetch)
        (uri "http://www.iconarchive.com/download/i105271/papirus-team/papirus-apps/amazon-store.svg")
        (sha256
          (base32
            "0c5j5dgbkblv5xnbbnxad8zw7fisqlm2lrknzf1mhbzv5v9k3adc"))))

(define-public amazon-prime-video
  (package
    (name "amazon-prime-video")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (native-inputs
      `(("amazon-icon" ,%amazon-icon)))
    (inputs
      `(("google-chrome" ,google-chrome)
	("bash" ,bash)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (mkdir-p (string-append (assoc-ref %outputs "out") "/bin"))
         (mkdir-p (string-append (assoc-ref %outputs "out") "/share/applications"))
	 (mkdir-p
	   (string-append
	     (assoc-ref %outputs "out")
             "/share/icons/hicolor/scalable/apps"))

         (copy-file
          (assoc-ref %build-inputs "amazon-icon")
          (string-append (assoc-ref %outputs "out")
                         "/share/icons/hicolor/scalable/apps/amazon-prime-video.svg"))

         (with-output-to-file
            (string-append (assoc-ref %outputs "out") "/bin/amazon-prime-video")
              (lambda _
                (format #t
                  "#!/bin/sh~@
                   ~a/bin/chrome --app=https://www.amazon.co.uk/Prime-Video/b/?node=3280626031&ref=atv_surl_piv~%"
                     (assoc-ref %build-inputs "google-chrome"))))

	 (chmod (string-append
		   (assoc-ref %outputs "out")
		   "/bin/amazon-prime-video")
		#o755)

         (with-output-to-file
            (string-append (assoc-ref %outputs "out") "/share/applications/amazon-prime-video.desktop")
              (lambda _
                (format #t
                  "[Desktop Entry]~@
                   Name=Amazon Prime Video~@
                   Comment=Watch TV shows and movies~@
                   Exec=~a/bin/amazon-prime-video~@
                   Icon=amazon-prime-video~@
                   Categories=AudioVideo;Network~@
                   Type=Application~%"
                     (assoc-ref %outputs "out"))))
            )
	 ))
  (home-page "https://www.amazon.com")
  (synopsis "A wrapper to view Amazon Prime Video.")
  (description "A wrapper to view Amazon Prime Video.")
  (license #f)))

(define-public netflix
  (package
    (name "netflix")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (native-inputs
      `(("netflix-icon" ,%netflix-icon)))
    (inputs
      `(("google-chrome" ,google-chrome)
	("bash" ,bash)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (mkdir-p (string-append (assoc-ref %outputs "out") "/bin"))
         (mkdir-p (string-append (assoc-ref %outputs "out") "/share/applications"))
	 (mkdir-p
	   (string-append
	     (assoc-ref %outputs "out")
             "/share/icons/hicolor/scalable/apps"))

         (copy-file
          (assoc-ref %build-inputs "netflix-icon")
          (string-append (assoc-ref %outputs "out")
                         "/share/icons/hicolor/scalable/apps/netflix.svg"))

         (with-output-to-file
            (string-append (assoc-ref %outputs "out") "/bin/netflix")
              (lambda _
                (format #t
                  "#!/bin/sh~@
                   ~a/bin/chrome --app=https://www.netflix.com~%"
                     (assoc-ref %build-inputs "google-chrome"))))

	 (chmod (string-append
		   (assoc-ref %outputs "out")
		   "/bin/netflix")
		#o755)

         (with-output-to-file
            (string-append (assoc-ref %outputs "out") "/share/applications/netflix.desktop")
              (lambda _
                (format #t
                  "[Desktop Entry]~@
                   Name=Netflix~@
                   Comment=Watch TV shows and movies~@
                   Exec=~a/bin/netflix~@
                   Icon=netflix~@
                   Categories=AudioVideo;Network~@
                   Type=Application~%"
                     (assoc-ref %outputs "out"))))
            )
	 ))
  (home-page "https://www.netflix.com")
  (synopsis "A wrapper to view Netflix.")
  (description "A wrapper to view Netflix.")
  (license #f)))


(package (inherit netflix))
