(define-module (pkill9 packages linux)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages tls)
  #:use-module (guix build-system trivial)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils) ;; version-major
  #:use-module (pkill9 utils) ;; create-inferior-package
  #:export (create-linux-package-data
            create-linux-package-inferior))

(define %boot-logo-patch
  ;; Linux-Libre boot logo featuring Freedo and a gnu.
  (origin
   (method url-fetch)
   (uri (string-append "http://www.fsfla.org/svn/fsfla/software/linux-libre/"
                       "lemote/gnewsense/branches/3.16/100gnu+freedo.patch"))
   (sha256
    (base32
     "1hk9swxxc80bmn2zd2qr5ccrjrk28xkypwhl4z0qx4hbivj7qm06"))))

(define* (create-linux-package-data #:key
                                    version source-checksum
                                    (name "linux"))
  (let* ((major-version (version-major version))
         (source-url (string-append
                      "https://"
                      "www.kernel.org"
                      "/pub/linux/kernel"
                      "/v" major-version ".x/"
                      "linux-" version ".tar.xz")))
    `(package (inherit
               (@ (gnu packages linux) linux-libre))
              (name ,name)
              (version ,version)
              (source (origin
                       (method url-fetch)
                       (uri ,source-url)
                       (sha256
                        (base32
                         ,source-checksum))))
              (synopsis "Mainline Linux kernel, nonfree binary blobs included.")
              (description "Linux is a kernel.")
              ;;(license license:gpl2) ;;needs an import of license
              (home-page "http://kernel.org/"))))

(define* (create-linux-package-inferior #:key
                                        version
                                        source-checksum
                                        guix-commit)
  (create-inferior-package #:guix-commit guix-commit
                           #:package-code-to-evaluate
                           (create-linux-package-data #:version version
                                                      #:source-checksum source-checksum)))
;;; Forgive me Stallman for I have sinned.

(define-public radeon-firmware-non-free
  (package
   (name "radeon-firmware-non-free")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/radeon/")))
                    (mkdir-p fw-dir)
                    (for-each (lambda (file)
                                (copy-file file
                                           (string-append fw-dir "/"
                                                          (basename file))))
                              (find-files source
                                          (lambda (file stat)
                                            (string-contains file "radeon"))))
                    #t))))

   (home-page "")
   (synopsis "Non-free firmware for Radeon integrated chips")
   (description "Non-free firmware for Radeon integrated chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.radeon_firmware;hb=HEAD"))))

(define-public ath10k-firmware-non-free
  (package
   (name "ath10k-firmware-non-free")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/")))
                    (mkdir-p fw-dir)
                    (copy-recursively (string-append source "/ath10k")
                                      (string-append fw-dir "/ath10k"))
                    #t))))

   (home-page "")
   (synopsis "Non-free firmware for ath10k wireless chips")
   (description "Non-free firmware for ath10k integrated chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.radeon_firmware;hb=HEAD"))))

(define-public linux-firmware-non-free
  (package
   (name "linux-firmware-non-free")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/")))
                    (mkdir-p fw-dir)
                    (copy-recursively source fw-dir)
                    #t))))

   (home-page "")
   (synopsis "Non-free firmware for Linux")
   (description "Non-free firmware for Linux")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.radeon_firmware;hb=HEAD"))))

(define-public iwlwifi-firmware-nonfree
  (package
   (name "iwlwifi-firmware-nonfree")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware")))
                    (mkdir-p fw-dir)
                    (for-each (lambda (file)
                                (copy-file file
                                           (string-append fw-dir "/"
                                                          (basename file))))
                              (find-files source "iwlwifi-.*\\.ucode$|LICENCE\\.iwlwifi_firmware$"))
                    #t))))

   (home-page "https://wireless.wiki.kernel.org/en/users/drivers/iwlwifi")
   (synopsis "Non-free firmware for Intel wifi chips")
   (description "Non-free firmware for Intel wifi chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.iwlwifi_firmware;hb=HEAD"))))

(define-public ibt-hw-firmware-nonfree
  (package
   (name "ibt-hw-firmware-nonfree")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/intel")))
                    (mkdir-p fw-dir)
                    (for-each (lambda (file)
                                (copy-file file
                                           (string-append fw-dir "/"
                                                          (basename file))))
                              (find-files source "ibt-hw-.*\\.bseq$|LICENCE\\.ibt_firmware$"))
                    #t))))

   (home-page "http://www.intel.com/support/wireless/wlan/sb/CS-016675.htm")
   (synopsis "Non-free firmware for Intel bluetooth chips")
   (description "Non-free firmware for Intel bluetooth chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.ibt_firmware;hb=HEAD"))))
